from __future__ import division

from model.Car import Car
from model.Game import Game
from model.Move import Move
from model.World import World
from model.TileType import TileType
from model.BonusType import BonusType

import numpy as np
from math import hypot, atan2
from collections import deque
from itertools import islice
from datetime import datetime

from PathFinder import TilePathFinder
from Tile import Tile
from Physics import Physics
from Map import Map
from helpers import *


class MyStrategy:
    SPEED_EPSILON = 0.2
    FIRE_ANGLE = 3.0 / 180.0 * np.pi
    BONUS_PICK_ANGLE_CONST = 30.0 / 180.0 * np.pi
    FIRE_DISTANCE = 3000.0
    SAFE_DISTANCE = 600.0
    BONUS_TURN_DISTANCE = 1600.0
    TRAJ_LEN = 60

    # 1: +pi/2, -1: -pi/2

    # TODO: add feature pairs? to turn wheels before feature
    patterns = {
        (1,0):{'name':"fast_turn", 'entrance': np.array([276,-280]), 'speed' : 25.5, 'dir': 1},
        (1,1):{'name':"u_turn", 'entrance': np.array([212,-235]), 'speed' : 24.5, 'dir': 1},
        (1,-1):{'name':"s_turn", 'entrance': np.array([10,-400]), 'speed' : 27.0, 'dir': 1},
        (0,0):{'name':"straight", 'entrance': np.array([400,0]), 'speed' : 100, 'dir': 1},
        (0,1):{'name':"turn_ahead", 'entrance': np.array([40,135]), 'speed' : 16.5, 'dir': 1}
    }

    def __init__(self):
        self.waypoint_index = -1
        self.laps_counter = 0
        self.turns = []
        self.feature_tile_index = 0
        self.bonuses_ahead = []


        # self.center_starting_position = None

        # self.next_waypoint = np.zeros(2)
        # self.angle_to_waypoint = 0
        # self.destination = np.zeros(2)

        self.prev_tile = None
        self.path_finder = None
        self.game = None
        self.tiles_x_y = None
        self.physics = None
        self.map = None

        self.path = deque()

        self.car_bounding_circle_radius = None
        self.car_forward_body_angle = None

        self.last_speed = np.zeros(2)
        self.last_speed_module = 0
        self.last_bonuses = []

        #ACTION related
        self.state = "go"
        self.previous_state = "go"
        self.evacuation_counter = 0
        # self.toggle = False

        self.wheel_turn = 0.0
        self.engine_power = 0.0
        self.brake = False

        self.predicted_pos = deque()

        self.wheel_sequence = np.zeros(self.TRAJ_LEN)
        self.brake_sequence = np.zeros(self.TRAJ_LEN, dtype = np.int8)
        self.traj = np.zeros(self.TRAJ_LEN)


    def move(self, me, world, game, move):
        if self.game is None:
            self.game = game

        #delete
        if self.tiles_x_y is None:
            self.tiles_x_y = np.array(world.tiles_x_y)

        if self.map is None:
            self.map = Map(world.tiles_x_y, game.track_tile_size, world.waypoints)

        if self.path_finder is None:
            self.path_finder = TilePathFinder(self.map, path_length = 10)


        if self.physics is None:
            self.physics = Physics(game, me)

        if self.car_bounding_circle_radius is None:
            self.car_bounding_circle_radius = hypot(game.car_width, game.car_height) * 0.5

        if self.car_forward_body_angle is None:
            self.car_forward_body_angle = atan2(game.car_height, game.car_width)

        curr_pos = np.array([me.x, me.y, me.angle])
        curr_speed = np.array([me.speed_x, me.speed_y])
        curr_speed_module = hypot(*curr_speed)

        # self.TRAJ_LEN = int(curr_speed_module*3) if curr_speed_module > 10 else 10

        # TODO: renew next keypoint not on tile entrance, but on some distance to prev keypoint
        entering_new_tile = False
        if len(self.path_finder.path) == 0 or not self.path_finder.path[0]['tile'].x_y_in(me.x, me.y):
            entering_new_tile = True
            self.last_lap = (self.laps_counter + 1 == game.lap_count)
            new_tile_pos = self.map.get_tile_by_xy(me.x, me.y).pos

            self.path_finder.update_tile_queue(new_tile_pos, me.next_waypoint_index, self.last_lap)
            first_in_path = self.path_finder.path[1]['tile'].pos

            if not(first_in_path == new_tile_pos).all():
                self.path_finder.update_tile_queue(new_tile_pos, me.next_waypoint_index, self.last_lap, renew_all = True)
                # self.update_tile_path(new_tile_pos, me.next_waypoint_index, self.last_lap, renew_all)

            key_tile = self.path_finder.get_tile_with_first_keypoint()
            # print key_tile
            self.next_key_point = key_tile['keypoint']
            # self.update_next_feature(me.x, me.y, me.next_waypoint_index)
            print self.next_key_point

        # bonuses = world.bonuses
        # if self.bonus_picked(bonuses) or entering_new_tile:
        #     self.last_bonuses = bonuses
        #     self.bonuses_ahead = self.get_bonuses_ahead(bonuses, me)
        # self.next_key_point =
        angle_to_keypoint = me.get_angle_to(*self.next_key_point)
        # angle = angle_to_keypoint
        distance_to_keypoint = me.get_distance_to(*self.next_key_point)

        # if (world.tick > game.initial_freeze_duration_ticks and
        #         self.state == "go" and
        #         curr_speed_module <= self.last_speed_module < self.SPEED_EPSILON and
        #         self.evacuation_counter == 0
        #         ):
        #     self.state = "reverse"
        #     self.evacuation_counter = 55
        #     self.crash_point = [me.x,me.y]
        #     self.crash_angle_to_KP = angle_to_keypoint
        #     self.reverse_direction = None


        # if self.center_starting_position is None:
        #     rot = int(me.angle/np.pi)
        #     if rot in self.rotation:
        #         x_in_tile = me.x - self.path[0].coords[0,0]
        #         y_in_tile = me.y - self.path[0].coords[1,0]
        #         rotated_position = np.absolute(np.dot(self.rotation[rot], np.array([x_in_tile, y_in_tile]))) # without actual direction
        #         self.center_starting_position = True if np.absolute(rotated_position[0] - game.track_tile_size / 2) < 200.0 else False
        #     else:
        #         self.center_starting_position = False

        #bonuses use
        # if world.tick > game.initial_freeze_duration_ticks:
        #     straight = self.get_straight_length()
        #     if straight > 4 and np.absolute(me.angle - angle_to_keypoint < 15.0):
        #         # do not nitro from center at start
        #         # if not self.center_starting_position or world.tick > game.initial_freeze_duration_ticks + 70:
        #         move.use_nitro = True
        #     if self.next_feature['name'] == 'fast_turn' and distance_to_keypoint < 400:
        #         move.spill_oil = True
        #
        #     if any(map(lambda car: not car.player_id == me.player_id and
        #             np.absolute(me.get_angle_to_unit(car)) < self.FIRE_ANGLE and
        #             me.get_distance_to_unit(car) < self.FIRE_DISTANCE and
        #             car.durability > 0.0, world.cars)):
        #         move.throw_projectile = True

        # if self.state == "go":
        # self.wheel_turn = 0.0
        move.engine_power = 1.0
        move.wheel_turn = self.wheel_sequence[0]
        move.brake = self.brake_sequence[0]


        #
        dtime = datetime.now()
        t0 = dtime.microsecond
        self.traj = self.physics.estimate_car_trajectory(self.TRAJ_LEN, curr_pos, curr_speed, me.angular_speed,
                                                    self.brake_sequence, self.wheel_sequence, move.engine_power)




        # if any(map(lambda point: self.map.x_y_inside_wall(point[0], point[1]), traj)):
        # if self.map.x_y_inside_wall(self.traj[-1,0], self.traj[-1,1]):
        # print self.map.get_wall_collision_of_rect_by_angle(680, 680, 0,
        #                                         self.car_forward_body_angle, self.car_bounding_circle_radius)

        #TODO: stop calculating trajectory in case of collision
        danger = False
        safe_trajectory_end_index = self.TRAJ_LEN
        for i, point in enumerate(self.traj):
            ends_near_wall = self.map.get_wall_colision_of_circle(point[0], point[1], self.car_bounding_circle_radius)
            if ends_near_wall is None or not ends_near_wall['wall'] is None:
                danger= True
                safe_trajectory_end_index = i
                break

        if danger == True:
            wall_collision = None
            for i,point in enumerate(self.traj[safe_trajectory_end_index:]):
                wall_collision = self.map.get_wall_collision_of_rect_by_angle(point[0], point[1], point[2],
                                                                self.car_forward_body_angle, self.car_bounding_circle_radius, only_forwards = False)
                if not wall_collision is None:
                    # print "out of path ", self.traj[-1], move.wheel_turn
                    tile = wall_collision['tile']
                    # print tile.turn, tile.pos
                    a = angle_to_keypoint - me.get_angle_to(point[0], point[1])
                    # print angle_to_keypoint, me.get_angle_to(point[0], point[1]), a

                    #TODO: put wheel_sequence and brake sequence mutations into separate function?

                    if a > 0 and self.wheel_sequence[0] < 1.0:
                        wheel_inc = game.car_wheel_turn_change_per_tick
                    elif a < 0 and self.wheel_sequence[0] > -1.0:
                        wheel_inc = -game.car_wheel_turn_change_per_tick
                    else:
                        wheel_inc = 0
                    self.wheel_sequence += wheel_inc
                    np.roll(self.wheel_sequence,1)
                    self.wheel_sequence[-1]=0.0
                    if i + safe_trajectory_end_index < curr_speed_module * 2.2 and curr_speed_module > 10:
                        self.brake_sequence = np.ones(self.TRAJ_LEN, dtype = np.int8) #BAD
                    else:
                        self.brake_sequence = np.zeros(self.TRAJ_LEN, dtype = np.int8) #BAD
                    break
            if wall_collision is None:
                self.brake_sequence = np.zeros(self.TRAJ_LEN, dtype = np.int8) #BAD
        else:
            self.brake_sequence = np.zeros(self.TRAJ_LEN, dtype = np.int8) #BAD


        move.wheel_turn = angle_to_keypoint * 10


        # self.predicted_pos.append(self.traj[-1])
        # dtime = datetime.now()
        # print "error = ",self.predicted_pos[0] - curr_pos,", time = ", dtime.microsecond - t0
        # if len(self.predicted_pos) >= self.TRAJ_LEN + 1:
        #     self.predicted_pos.popleft()


        # print dtime.microsecond - t0
        # print anymap(lambda point: self.physics.position_out(point), traj)
        # TODO: check not inside wall, but outside path
        # if any(map(lambda point: self.map.x_y_inside_wall(point[0], point[1]), traj)):
        #     print "brake"
        #     self.brake = True
        # else:
        #     self.brake = False
            # print traj[-1]
            # print traj

            # # pick a bonus
            # if distance_to_keypoint > self.SAFE_DISTANCE and curr_speed_module > 5.0 and not self.next_feature['name'] == 's_turn':
            #     safe_angle = self.BONUS_PICK_ANGLE_CONST
            #     bonuses_prioretised = []
            #     for bonus in self.bonuses_ahead:
            #         angle_to_bonus = me.get_angle_to_unit(bonus['unit'])
            #         if (np.absolute(angle_to_bonus - angle_to_keypoint) < safe_angle):
            #             bonuses_prioretised.append({
            #                 'priority': bonus['priority'],
            #                 'distance': me.get_distance_to_unit(bonus['unit']),
            #                 'angle': angle_to_bonus
            #             })
            #
            #     chosen_bonus_value = 0.0
            #     chosen_bonus = None
            #     for bonus in bonuses_prioretised:
            #         bonus_value = bonus['priority'] / (np.absolute(bonus['angle']) + 0.001)
            #         if bonus_value > 1.0 and bonus_value > chosen_bonus_value:
            #             chosen_bonus_value = bonus_value
            #             chosen_bonus = bonus
            #
            #
            #     if not chosen_bonus is None:
            #         if chosen_bonus['distance'] < self.BONUS_TURN_DISTANCE:
            #             angle = chosen_bonus['angle'] * 1.3
            #
            # self.wheel_turn = angle * 10.0
            #
            # if self.evacuation_counter > 0:
            #     dir_vec = np.array([np.cos(me.angle), np.sin(me.angle)])
            #     self.wheel_turn = 0.0
            #     self.brake=True
            #     speed_point_dir = dir_vec.dot([me.speed_x,me.speed_y])
            #     if speed_point_dir > -1.5:
            #         self.brake=False
            #         self.evacuation_counter -= 1
            #         self.wheel_turn = self.crash_angle_to_KP * 100

        # elif self.state == "reverse":
        #     stuck = False
        #     if self.evacuation_counter > 0:
        #         stuck = True
        #         self.evacuation_counter -=1
        #
        #     distance_to_crash_point = me.get_distance_to(*self.crash_point)
        #     angle_to_crash_point = me.get_angle_to(*self.crash_point)
        #     free_to_go = (distance_to_crash_point > 200.0 and
        #                 np.absolute(angle_to_crash_point - me.angle) > np.pi/8.0) or \
        #                 (not stuck and curr_speed_module < self.SPEED_EPSILON)
        #     if free_to_go:
        #         self.engine_power = 1.0
        #         self.evacuation_counter = 70
        #         self.state = "go"
        #         self.wheel_turn = 0
        #     else :
        #         self.brake = False
        #         self.engine_power = -1.0
        #         self.wheel_turn = -self.crash_angle_to_KP * 100

        if not self.waypoint_index == me.next_waypoint_index:
            if self.waypoint_index == 0:
                self.laps_counter += 1
            self.waypoint_index = me.next_waypoint_index

        self.last_speed = curr_speed
        self.last_speed_module = curr_speed_module
        # self.set_movement(move)


    def get_bonus_priority(self, bonus, me):
        if bonus.type == BonusType.PURE_SCORE:
            priority = 1.0
        elif bonus.type == BonusType.REPAIR_KIT:
            priority = (1.0 - me.durability if me.durability < 0.6 else 0.0)/1.5
        elif not self.last_lap: #TODO: estimate distance to end race
            if bonus.type == BonusType.NITRO_BOOST or bonus.type == BonusType.AMMO_CRATE:
                priority = 0.2
            elif bonus.type == BonusType.OIL_CANISTER:
                priority = 0.05
            else: #never happens
                priority = 0.05
        else:
            priority = 0.0
        return priority

    def bonus_picked(self, bonuses):
        if not len(self.last_bonuses) == len(bonuses):
            return True
        else:
            return False

    def get_bonuses_ahead(self, bonuses,me):
        bonuses_ahead = []
        for bonus in bonuses:
            for tile in islice(self.path, 0, 4): #4 tiles ahead
                if tile.x_y_in(bonus.x, bonus.y):
                    priority = self.get_bonus_priority(bonus,me)
                    if priority > 0.0:
                        bonuses_ahead.append({'unit':bonus, 'priority': priority})
        return bonuses_ahead


    def set_movement(self, move):
        if not move.wheel_turn == self.wheel_turn:
            move.wheel_turn = self.wheel_turn
        if not move.engine_power == self.engine_power:
            move.engine_power = self.engine_power
        if not move.brake == self.brake:
            move.brake = self.brake
