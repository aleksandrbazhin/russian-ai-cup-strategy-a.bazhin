from __future__ import division

import numpy as np
from math import cos, sin

from model.TileType import TileType

from Tile_characteristics import tile_neighbours
from Tile import Tile

# all map with tiles as objects
class Map:
    def __init__(self, tiles_x_y, tile_size, waypoints):
        self.tiles_np_array = np.array(tiles_x_y)
        self.tiles = []
        for i,tile_row in enumerate(tiles_x_y):
            temp_row = []
            for j,tile_type in enumerate(tile_row):
                temp_row.append(Tile([i,j], tile_type, tile_size))
            self.tiles.append(temp_row)
        # print self.tiles
        self.tile_size = tile_size
        self.waypoints = waypoints
        self.full_size = np.array([len(tiles_x_y[0]) * tile_size, len(tiles_x_y) * tile_size])

    def get_tile_by_pos(self, x, y):
        return self.tiles[x][y] if 0 <= x < self.full_size[0] and 0 <= y < self.full_size[1] else None

    def get_tile_by_xy(self, x, y):
        if x < 0 or x > self.full_size[0] or y < 0 or y > self.full_size[1]:
            return None
        # print int(x // self.tile_size), int(y // self.tile_size)
        return self.tiles[int(x // self.tile_size)][int(y // self.tile_size)]

    def x_y_inside_wall(self, x, y):
        tile = self.get_tile_by_xy(x, y)
        if tile.type == TileType.EMPTY:
            return True
        return (True if tile.get_wall_by_xy(x, y) else False) if not tile is None else True

    #gets wall of collision by body_angle and body_circle_raduis
    def get_wall_collision_of_rect_by_angle(self, center_x, center_y, angle, body_angle, body_circle_raduis, only_forwards = True):
        forward_left_x = center_x + cos(angle - body_angle) * body_circle_raduis
        forward_left_y = center_y + sin(angle - body_angle) * body_circle_raduis
        tile = self.get_tile_by_xy(forward_left_x, forward_left_y)
        # print forward_left_x, forward_left_y
        if not tile is None:
            wall = tile.get_wall_by_xy(forward_left_x, forward_left_y)
            if not wall is None:
                return {'tile': tile, 'wall': wall}
        forward_right_x = center_x + cos(angle + body_angle) * body_circle_raduis
        forward_right_y = center_y + sin(angle + body_angle) * body_circle_raduis
        tile = self.get_tile_by_xy(forward_right_x, forward_right_y)
        # print forward_left_x, forward_left_y, forward_right_x, forward_right_y
        if not tile is None:
            wall = tile.get_wall_by_xy(forward_right_x, forward_right_y)
            if not wall is None:
                return {'tile': tile, 'wall': wall}
        if not only_forwards:
            backwards_left_x = center_x + cos(angle + body_angle + np.pi) * body_circle_raduis
            backwards_left_y = center_y + sin(angle + body_angle + np.pi) * body_circle_raduis
            tile = self.get_tile_by_xy(backwards_left_x, backwards_left_y)
            # print forward_left_x, forward_left_y
            if not tile is None:
                wall = tile.get_wall_by_xy(backwards_left_x, backwards_left_y)
                if not wall is None:
                    return {'tile': tile, 'wall': wall}
            backwards_right_x = center_x + cos(angle - body_angle + np.pi) * body_circle_raduis
            backwards_right_y = center_y + sin(angle - body_angle + np.pi) * body_circle_raduis
            tile = self.get_tile_by_xy(backwards_right_x, backwards_right_y)
            if not tile is None:
                wall = tile.get_wall_by_xy(backwards_right_x, backwards_right_y)
                if not wall is None:
                    return {'tile': tile, 'wall': wall}
        return None

    def get_wall_colision_of_circle(self, x, y, r):
        tile = self.get_tile_by_xy(x, y)
        return {'tile': tile, 'wall': tile.get_wall_colision_of_circle(x,y,r)} if not tile is None else None
