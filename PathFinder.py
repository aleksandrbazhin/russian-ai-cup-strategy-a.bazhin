from __future__ import division
# finds tile path

import numpy as np
from collections import deque
from itertools import islice

from model.TileType import TileType

from Tile_characteristics import tile_neighbours, tile_walls, TILE_CORNERS
from model.TileType import TileType
from Map import Map
from Tile import Tile
from helpers import *

class TilePathFinder:
    # in all patterns 1 turn goes first, otherwise pattern is inverted
        # patterns = {
        #     (1,0):{'name':"fast_turn", 'entrance': np.array([276,-280]), 'speed' : 25.5, 'dir': 1},
        #     (1,1):{'name':"u_turn", 'entrance': np.array([212,-235]), 'speed' : 24.5, 'dir': 1},
        #     (1,-1):{'name':"s_turn", 'entrance': np.array([10,-400]), 'speed' : 27.0, 'dir': 1},
        #     (0,0):{'name':"straight", 'entrance': np.array([400,0]), 'speed' : 100, 'dir': 1},
        #     (0,1):{'name':"turn_ahead", 'entrance': np.array([40,135]), 'speed' : 16.5, 'dir': 1}
        # }
    feature_patterns = {
        (0,0,0):{'name':"straight", "point" : None},
        (0,0,1):{'name':"turn_in_1", "point" : np.array([200,200])},
        (0,1,0):{'name':"fast_turn", "point" : np.array([100,280])},
        (1,0,0):{'name':"turn_out", "point" : np.array([-270,270])},
        (0,1,1):{'name':"u_turn", "point" : np.array([200,200])},
        (1,1,0):{'name':"u_turn_out", "point" : np.array([250,-250])},
        (1,0,1):{'name':"pi_turn", "point" : np.array([0,0])},
        (0,1,-1):{'name':"s_turn_1", "point" : np.array([250,250])},
        (1,-1,0):{'name':"s_turn_2", "point" : np.array([250,-250])},
        (1,0,-1):{'name':"s_turn_long", "point" : None},
        (1,-1,-1):{'name':"s_u_turn", "point" : np.array([100,100])},
        (1,1,-1):{'name':"u_s_turn", "point" : np.array([300,0])},
        (1,-1,1):{'name':"double_s_turn", "point" : None},
        (1,1,1):{'name':"double_u_turn", "point" : np.array([0,0])},
    }
    def __init__(self, map, path_length = 6):
        self.path = deque()
        self.map = map
        self.tiles = map.tiles_np_array
        self.waypoints = map.waypoints
        self.path_length = path_length
        self.path_end = 1
        self.curr_tile_index = np.array([0,0])

    # find path_length tiles ahead
    # returns: deque with new added tiles
    def update_tile_queue(self, current, next_waypoint_index, last_lap = False, renew_all = False):
        if renew_all:
            self.path.clear()
            self.path_end = next_waypoint_index
        old_path_length = len(self.path)
        #refactor
        is_new_path = old_path_length == 0
        if is_new_path:
            new_path = self.find_path(current, self.waypoints[next_waypoint_index])
            self.path.extend(new_path)#remove
            self.last_waypoint_in_path = next_waypoint_index
        else:
            new_path = [] #remove
            self.curr_tile_index = self.path.popleft()

        while len(self.path) < self.path_length or (self.path_end + 1 >= len(self.waypoints) and last_lap):
            next_index = self.path_end + 1 if self.path_end + 1 < len(self.waypoints) else 0
            path_part = self.find_path( self.waypoints[self.path_end], self.waypoints[next_index] )[1:]
            new_path.extend( path_part )#remove
            self.path.extend( path_part )
            self.path_end = next_index
        if not old_path_length == len(self.path):
            self.update_turns_in_path(is_new_path)
            # self.update_point_path(old_path_length)

        if old_path_length == 0:
            self.path[0]['in_vector'] = self.path[1]['in_vector']
            self.path[0]['in_angle'] = self.path[1]['in_angle']
            self.path[0]['turn_angle'] = 0
        # print self.path
        return new_path#remove



    def update_turns_in_path(self, is_new_path = False):
        #compute in vectors
        pos = 1 # computes only for tiles without 'in_vector'
        while pos < len(self.path): #straight
            if not 'in_vector' in self.path[pos]:
                self.path[pos]['in_vector'] = self.get_in_vector(pos)
            if not 'in_angle' in self.path[pos]:
                self.path[pos]['in_angle'] = angles_div_pi_2[tuple(self.path[pos]['in_vector'])]
            pos += 1
        #compute turn angles
        pos = 1
        while pos < len(self.path) - 1: #straight
            if not 'turn_angle' in self.path[pos]:
                a = self.get_tile_turn(pos)
                if not a is None:
                    self.path[pos]['turn_angle'] = self.get_tile_turn(pos)
            pos += 1

        if is_new_path:
            # find angles in first tile
            self.path[0]['in_vector'] = self.path[1]['in_vector']
            self.path[0]['in_angle'] = self.path[1]['in_angle']
            self.path[0]['turn_angle'] = 0

        #compute keypoints
        pos = 1
        self.path[-1]['keypoint'] = self.path[-1]['tile'].center # temporary keypoint at path end
        while pos < len(self.path) - 2: #straight
            if not 'feature' in self.path[pos]:
                prev_tile = self.path[pos - 1]
                tile = self.path[pos]
                next_tile =  self.path[pos + 1]
                pattern = np.array([prev_tile['turn_angle'], tile['turn_angle'], next_tile['turn_angle']])
                #invert pattern
                pattern_inversion = 1
                if prev_tile['turn_angle'] == -1 or \
                        (prev_tile['turn_angle'] == 0 and tile['turn_angle'] == -1) or \
                        (prev_tile['turn_angle'] == tile['turn_angle']  == 0 and next_tile['turn_angle'] == -1) :
                    print pattern
                    pattern = -pattern
                    pattern_inversion = -1
                feature = self.feature_patterns[tuple(pattern)]
                if feature['point'] is None:
                    tile['keypoint'] = None
                else:
                    abs_rotation_matrice = rotation_matrices[tile['in_angle']]
                    point = feature['point'] * np.array([1, pattern_inversion])
                    tile['keypoint'] = tile['tile'].center + np.dot(abs_rotation_matrice, point)
                    # print tile['in_angle'], abs_rotation_matrice, tile['turn_angle'], point, np.dot(abs_rotation_matrice, point)
                tile['feature'] = feature
                # if not tile['keypoint'] is None:
                #     print tile['keypoint'], tile['feature']['name'], "center=", tile['tile'].center
            pos += 1


    def get_tile_with_first_keypoint(self):
        kp = None
        i = 0
        while kp is None:
            kp = self.path[i]['keypoint'] if 'keypoint' in self.path[i] else None
            i += 1
        return self.path[i - 1]

    def get_in_vector(self, pos):
        return self.path[pos]['tile'].pos - self.path[pos - 1]['tile'].pos

    def get_tile_turn(self, pos):
        if 'in_vector' in self.path[pos] and 'in_vector' in self.path[pos + 1]:
            v_in = self.path[pos]['in_vector']
            v_out = self.path[pos + 1]['in_vector']
            return int(np.cross(v_in, v_out))
        else:
            return None

    # heuristic estimation - min distance + angle to waypoint
    def get_weight(self, from_tile, destination, origin):
        diff = from_tile - destination
        vector_to_destination = np.arctan2(diff[1],diff[0])
        close_to_pi_div_2 = (1.0 + np.absolute(vector_to_destination // (np.pi/2) ) * 0.2)
        return self.distance(from_tile, destination) / close_to_pi_div_2
        # return self.distance(from_tile, destination) + self.distance(from_tile, origin) / close_to_pi_div_2

    def distance(self, from_tile, to):
        diff = from_tile - to
        return diff.dot(diff)

    def tile_in(self, tile, tset):
        for t in tset:
            if (tile == t).all():
                return True
        return False

    def reconstruct_path(self, came_from, current):
        #TODO: add tile turn
        total_path = [{'tile': self.map.get_tile_by_pos(*tuple(current))}]
        while not (came_from[tuple(current)] == [1000,1000]).all():
            current = came_from[tuple(current)]
            total_path.append({'tile': self.map.get_tile_by_pos(*tuple(current))})
        total_path.reverse()
        return total_path

    def distance_to_neighbour(self, new_tile, curr_tile, prev_tile = None, prev_prev_tile = None):
        if not prev_tile is None:
            v_in  = curr_tile - prev_tile
            v_out = new_tile - curr_tile
            if (v_in == v_out).all():
                return 0.9
            if (-v_in == v_out).all():
                return 20.0
            if not prev_prev_tile is None:
                v_in_prev = prev_tile - prev_prev_tile
                if (v_out == prev_prev_tile).all():
                    return 3.0
        return 1.0


    def find_path(self, from_index, to_index):
        # convenience vars
        curr_index_t = tuple(from_index)
        to = np.array(to_index)
        from_np = np.array(from_index)

        open_set = [from_np]
        if len(self.path) > 0:
            closed_set = [self.path[-1]]
        else:
            closed_set = []


        came_from = np.full((self.tiles.shape[0], self.tiles.shape[1], 2), 1000, dtype = np.int_)

        g_score = np.full_like(self.tiles, np.inf)
        g_score[ curr_index_t ] = 0
        f_score = np.full_like(self.tiles, np.inf)
        f_score[ curr_index_t  ] = g_score[ curr_index_t ] + self.get_weight(from_np, to, from_np)

        while len(open_set) > 0:
            curr_index = min(open_set, key = lambda tile: f_score[tuple(tile)])
            curr_index_t = tuple(curr_index)
            previous_tile = came_from[curr_index_t] if curr_index_t in came_from else (
                self.path[-2] if len(self.path) >= 2 and (self.path[-1] == curr_index).all() else None)
            if not previous_tile is None:
                previous_tile_t = tuple(previous_tile)
                previous_previous_tile = came_from[previous_tile_t] if previous_tile_t in came_from else (
                    self.path[-3] if len(self.path) >= 3 and (self.path[-2] == previous_tile_t).all() else None)
            else:
                previous_previous_tile = None
            if (curr_index == to_index).all():
                return self.reconstruct_path(came_from, to)
            closed_set.append(curr_index)
            remove_np_array(open_set, curr_index)
            neighbours = curr_index + tile_neighbours[ self.tiles[ curr_index_t ] ]
            for tile in neighbours:
                tile_t = tuple(tile)
                if self.tile_in(tile, closed_set):
                    continue
                tentative_g_score = g_score[curr_index_t] + self.distance_to_neighbour(tile, curr_index, previous_tile, previous_previous_tile)
                if not self.tile_in(tile, open_set):
                    open_set.append(tile)
                elif tentative_g_score >= g_score[tile_t]:
                    continue
                came_from[tile_t] = curr_index
                g_score[tile_t] = tentative_g_score
                f_score[tile_t] = g_score[tile_t] + self.get_weight(tile, to, from_np)
