from model.TileType import TileType
import numpy as np

tile_neighbours = {
    TileType.EMPTY : np.array([[0,0]]),
    TileType.VERTICAL : np.array([[0,1],[0,-1]]),
    TileType.HORIZONTAL : np.array([[1,0],[-1,0]]),
    TileType.LEFT_TOP_CORNER : np.array([[1,0],[0,1]]),
    TileType.RIGHT_TOP_CORNER : np.array([[-1,0],[0,1]]),
    TileType.LEFT_BOTTOM_CORNER : np.array([[1,0],[0,-1]]),
    TileType.RIGHT_BOTTOM_CORNER : np.array([[-1,0],[0,-1]]),
    TileType.LEFT_HEADED_T : np.array([[-1,0],[0,1],[0,-1]]),
    TileType.RIGHT_HEADED_T : np.array([[1,0],[0,1],[0,-1]]),
    TileType.TOP_HEADED_T : np.array([[1,0],[-1,0],[0,-1]]),
    TileType.BOTTOM_HEADED_T : np.array([[1,0],[-1,0],[0,1]]),
    TileType.CROSSROADS : np.array([[1,0],[-1,0],[0,1],[0,-1]]),
    TileType.UNKNOWN : np.array([[1,0],[-1,0],[0,1],[0,-1]])
}


# turn_tiles  = [TileType.LEFT_TOP_CORNER, TileType.RIGHT_TOP_CORNER, TileType.LEFT_BOTTOM_CORNER, TileType.RIGHT_BOTTOM_CORNER]
# t_tiles     = [TileType.LEFT_HEADED_T, TileType.RIGHT_HEADED_T, TileType.TOP_HEADED_T, TileType.BOTTOM_HEADED_T]
# cross_tiles = [TileType.CROSSROADS]



# 'straights':[[-x,x],[-y,y]], boolean

TILE_CORNERS = {'left_top':1,
                'right_top':2,
                'left_bottom':3,
                'right_bottom':4}
# 'rounds':[corner1,...],
tile_walls = {
    TileType.EMPTY : {'straights':np.array([[True,True], [True,True]]),
                        'rounds':[]},
    TileType.VERTICAL : {'straights':np.array([[True,True],[False,False]]),
                        'rounds':[]},
    TileType.HORIZONTAL : {'straights':np.array([[False,False],[True,True]]),
                        'rounds':[]},
    TileType.LEFT_TOP_CORNER : {'straights':np.array([[True,False],[True,False]]),
                        'rounds':[TILE_CORNERS['right_bottom']]},
    TileType.RIGHT_TOP_CORNER : {'straights':np.array([[False,True],[True,False]]),
                        'rounds':[TILE_CORNERS['left_bottom']]},
    TileType.LEFT_BOTTOM_CORNER : {'straights':np.array([[True,False],[False,True]]),
                        'rounds':[TILE_CORNERS['right_top']]},
    TileType.RIGHT_BOTTOM_CORNER : {'straights':np.array([[False,True],[False,True]]),
                        'rounds':[TILE_CORNERS['left_top']]},
    TileType.LEFT_HEADED_T : {'straights':np.array([[True,False],[False,False]]),
                        'rounds':[TILE_CORNERS['right_bottom'], TILE_CORNERS['right_top']]},
    TileType.RIGHT_HEADED_T : {'straights':np.array([[False,True],[False,False]]),
                        'rounds':[TILE_CORNERS['left_bottom'], TILE_CORNERS['left_top']]},
    TileType.TOP_HEADED_T : {'straights':np.array([[False,False],[True,False]]),
                        'rounds':[TILE_CORNERS['left_bottom'], TILE_CORNERS['right_bottom']]},
    TileType.BOTTOM_HEADED_T : {'straights':np.array([[False,False],[False,True]]),
                        'rounds':[TILE_CORNERS['left_top'],TILE_CORNERS['left_bottom']]},
    TileType.CROSSROADS : {'straights':np.array([[False,False],[False,False]]),
                        'rounds':[TILE_CORNERS['left_top'],TILE_CORNERS['left_bottom'],TILE_CORNERS['right_top'],TILE_CORNERS['right_bottom']]},
    TileType.UNKNOWN : {'straights':np.array([[False,False],[False,False]]),
                        'rounds':[]},
}
