import numpy as np
from math import hypot

from Tile_characteristics import tile_neighbours, tile_walls, TILE_CORNERS
from model.TileType import TileType

TILE_WALL_SIZE = 80
TILE_ROUND_SIZE = 80
class Tile:
    def __init__(self, position, tile_type, tile_size, turn = None):
        self.pos = np.array(position, dtype =  np.int_)
        self.type = tile_type
        self.neighbours_reachable = tile_neighbours[tile_type]
        self.walls_straight = tile_walls[tile_type]['straights']
        self.walls_rounds = tile_walls[tile_type]['rounds']
        self.size = tile_size
        self.center = (self.pos + 0.5) * tile_size
        self.coords = np.array([self.pos, self.pos + 1]).T * tile_size
        self.turn = turn

    def __repr__(self):
        return "[{},{}] ".format(*self.pos)

    def x_y_in(self, x, y):
        return self.coords[0,0] < x < self.coords[0,1] and self.coords[1,0] < y < self.coords[1,1]

# MULTIPLE RETURNS - OUR CHOICE
    def get_wall_by_xy(self, x, y):
        if not self.x_y_in(x,y):
            return None
        # if self.type == TileType.EMPTY:
        #     return None
# TODO: profile and refactor conditions if needed
        max_wall_pos = self.size - TILE_WALL_SIZE
        x_inside_tile = x - self.coords[0,0]
        y_inside_tile = y - self.coords[1,0]
        x_out_left = x_inside_tile < TILE_WALL_SIZE
        x_out_right = x_inside_tile > max_wall_pos
        y_out_top = y_inside_tile < TILE_WALL_SIZE
        y_out_bottom = y_inside_tile > max_wall_pos
        # out of walls
        if not (x_out_left or x_out_right or y_out_top or y_out_bottom):
            return None
        # inside horizontal walls
        if self.walls_straight[0,0] and x_out_left:
            return (-1,0)
        if self.walls_straight[0,1] and x_out_right:
            return (1,0)
        # inside vertical walls
        if self.walls_straight[1,0] and y_out_top:
            return (0,-1)
        if self.walls_straight[1,1] and y_out_bottom:
            return (0,1)
        # inside corner rounds
        if x_out_left and y_out_top and TILE_CORNERS['left_top'] in self.walls_rounds and \
                hypot(x_inside_tile, y_inside_tile) < TILE_ROUND_SIZE:
            return (-1,-1)
        if x_out_left and y_out_bottom and TILE_CORNERS['left_bottom'] in self.walls_rounds and \
                hypot(x_inside_tile, self.size - y_inside_tile) < TILE_ROUND_SIZE:
            return (-1,1)
        if x_out_right and y_out_top and TILE_CORNERS['right_top'] in self.walls_rounds and \
                hypot(self.size - x_inside_tile, y_inside_tile) < TILE_ROUND_SIZE:
            return (1,-1)
        if x_out_right and y_out_bottom and TILE_CORNERS['right_bottom'] in self.walls_rounds and \
                hypot(self.size - x_inside_tile, self.size - y_inside_tile) < TILE_ROUND_SIZE:
            return (1,1)
        return None

    def get_wall_colision_of_circle(self, x, y, r):
        min_wall_collision_pos = TILE_WALL_SIZE + r
        max_wall_collision_pos = self.size - min_wall_collision_pos
        x_inside_tile = x - self.coords[0,0]
        y_inside_tile = y - self.coords[1,0]
        x_out_left = x_inside_tile < min_wall_collision_pos
        x_out_right = x_inside_tile > max_wall_collision_pos
        y_out_top = y_inside_tile < min_wall_collision_pos
        y_out_bottom = y_inside_tile > max_wall_collision_pos
        if not (x_out_left or x_out_right or y_out_top or y_out_bottom):
            return None
        if self.walls_straight[0,0] and x_out_left:
            return (-1,0)
        if self.walls_straight[0,1] and x_out_right:
            return (1,0)
        if self.walls_straight[1,0] and y_out_top:
            return (0,-1)
        if self.walls_straight[1,1] and y_out_bottom:
            return (0,1)
        # collision with corner
        if x_out_left and y_out_top and TILE_CORNERS['left_top'] in self.walls_rounds and \
                hypot(x_inside_tile, y_inside_tile) < TILE_ROUND_SIZE + r:
            return (-1,-1)
        if x_out_left and y_out_bottom and TILE_CORNERS['left_bottom'] in self.walls_rounds and \
                hypot(x_inside_tile, self.size - y_inside_tile) < TILE_ROUND_SIZE + r:
            return (-1,1)
        if x_out_right and y_out_top and TILE_CORNERS['right_top'] in self.walls_rounds and \
                hypot(self.size - x_inside_tile, y_inside_tile) < TILE_ROUND_SIZE + r:
            return (1,-1)
        if x_out_right and y_out_bottom and TILE_CORNERS['right_bottom'] in self.walls_rounds and \
                hypot(self.size - x_inside_tile, self.size - y_inside_tile) < TILE_ROUND_SIZE + r:
            return (1,1)
        return None
