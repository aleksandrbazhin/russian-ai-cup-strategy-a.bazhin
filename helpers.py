import numpy as np

def limit(a,b):
    return np.clip(a,-b,b)

def perpendicular( a ) :
    b = np.empty_like(a)
    b[0] = a[1]
    b[1] = -a[0]
    return b

def sumSqr(a,b):
    return a*a+b*b

def remove_np_array(L, arr):
    ind = 0
    size = len(L)
    while ind < size :
        if np.array_equal(L[ind],arr):
            L.pop(ind)
            size -= 1
        else :
            ind += 1

rotation = {1  : np.array([[0,-1],[1,0]]),
                      2  : np.array([[-1,0],[0,-1]]),
                      0  : np.array([[1,0],[0,1]]),
                      -1 : np.array([[0,1],[-1,0]])}

rotation_matrices = {-1  : np.array([[0,-1],[1,0]]),
                      0  : np.array([[-1,0],[0,-1]]),
                      2  : np.array([[1,0],[0,1]]),
                      1 : np.array([[0,1],[-1,0]])}

angles_div_pi_2 = {(1,0) : 0,
                   (0,1) : 1,
                   (-1,0): 2,
                   (0,-1):-1}
