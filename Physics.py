from math import pow, sin, cos, hypot
import numpy as np
# from datetime import datetime
from model.CarType import CarType
from helpers import *

class Physics:
    def __init__(self, game, car):
        # self.game = game
        self.car_width = game.car_width
        self.car_height = game.car_height
        if car.type == CarType.BUGGY :
            self.car_mass = game.buggy_mass
            self.max_engine_power = game.buggy_engine_forward_power
            self.max_engine_rear_power = game.buggy_engine_forward_power
        else :
            self.car_mass = game.jeep_mass
            self.max_engine_power = game.jeep_engine_forward_power
            self.max_engine_rear_power = game.jeep_engine_rear_power

        self.car_max_torque = self.max_engine_power / self.car_mass
        self.circumcircleRadius = hypot(self.car_width, self.car_height) / 2.0
        self.angularMassFactor = sumSqr(self.car_width, self.car_height) / 12.0
        # angularMass = self.car_mass * self.angularMassFactor
        # rotAccel = self.max_engine_power / angularMass * dt
        self.car_rot_factor = game.car_angular_speed_factor
        self.car_air_friction_factor = game.car_movement_air_friction_factor
        self.car_lengthwise_friction_factor = game.car_lengthwise_movement_friction_factor
        self.car_crosswise_friction_factor = game.car_crosswise_movement_friction_factor
        self.car_rotation_air_friction_factor = game.car_rotation_air_friction_factor
        self.car_rotation_friction_factor = game.car_rotation_friction_factor



    def estimate_car_trajectory(self, ticks_ahead, position, speed, angular_speed, brake_sequence, wheel_sequence, engine_power):

        # move.engine_power = self.EP
        # move.wheel_turn = self.WT
        # move.brake = self.BR



        iter_per_tick = 1
        dt = 1.0 / iter_per_tick
        car_max_acceleration = self.car_max_torque * dt
        air_friction_multiplier = pow(1 - self.car_air_friction_factor, dt)
        cross_friction = self.car_crosswise_friction_factor * dt
        long_friction = self.car_lengthwise_friction_factor * dt
        rot_air_friction_multiplier = pow(1 - self.car_rotation_air_friction_factor, dt)
        angular_friction = self.car_rotation_friction_factor * dt


        #     print "[",me.x,me.y,"], ticks=",ticks_ahead, " dt=",iter_per_tick
            # forecast position

            # dtime = datetime.now()
            # t0 = dtime.microsecond
        pos = np.copy(position[:2])
        angle = position[2]
        spd = np.copy(speed)
        angle = angle
        ang_velocity = angular_speed
        power = engine_power # by default
        med_ang_velocity = 0.0
        direction = np.array([np.cos(angle), np.sin(angle)])
        # if power does not change
        #    power += game.car_engine_power_change_per_tick
        accel = car_max_acceleration * power * direction

        trajectory = np.zeros([ticks_ahead, 3])
        for i in xrange(len(trajectory)):
            wheel_turn = wheel_sequence[i]
            if brake_sequence[i] == 1:
                long_friction = cross_friction
            ang_velocity -= med_ang_velocity
            med_ang_velocity = self.car_rot_factor * wheel_turn * (np.dot(spd, direction))
            ang_velocity += med_ang_velocity
            for j in xrange(iter_per_tick):
                pos += spd * dt
                spd += accel
                spd = air_friction_multiplier * spd
                spd -= limit(spd * direction, long_friction) * direction \
                     + limit(np.dot(spd, perpendicular(direction)), cross_friction) * perpendicular(direction)

                direction[0] = cos(angle)
                direction[1] = sin(angle)
                angle += ang_velocity * dt
                ang_velocity -= limit(ang_velocity - med_ang_velocity, angular_friction)
            trajectory[i,:2] = np.copy(pos)
            trajectory[i,2] = angle
        return trajectory


            # if self.position_out(pos):
            #     ta = me.get_angle_to(me.next_waypoint_x, me.next_waypoint_y)
            #     self.WT = -ta * 10.0
            # else:
            #     self.WT = 0
    #
    # def position_out(self,pos):
    #     return pos[1] < 150
    #         # dtime = datetime.now()
            # print pos, spd, (dtime.microsecond-t0)
        #
        # if game.initial_freeze_duration_ticks <= world.tick <= game.initial_freeze_duration_ticks + ticks_ahead:
        #     # print me.angle
        #     pass
        # #     print me.x, me.y, ' v = ', me.speed_x, me.speed_y
        #     # print "accel =", me.speed_y - self.prev_spd
        #     # self.prev_spd = me.speed_y
        # if world.tick == game.initial_freeze_duration_ticks + ticks_ahead:
        #     print me.x, me.y, '    v = ', me.speed_x, me.speed_y #, me.angle, me.angular_speed


                        # for i in xrange(ticks_ahead):
                        #     ang_velocity -= baseang_velocity;
                        #     baseang_velocity = car_rot_factor * wheel_turn * (np.dot(spd, direction))
                        #     ang_velocity += baseang_velocity;
                        #     for j in xrange(iter_per_tick):
                        #         pos += spd * dt
                        #         spd += accel
                        #         spd = air_friction_multiplier * spd
                        #         spd -= limit(spd * direction, long_friction) * direction \
                        #              + limit(np.dot(spd, perpendicular(direction)), cross_friction) * perpendicular(direction)
                        #         direction = np.array([np.cos(angle), np.sin(angle)])
                        #         angle += ang_velocity * dt
                        #         ang_velocity = baseang_velocity + (ang_velocity - baseang_velocity) * rot_air_friction_multiplier
                        #         ang_velocity -= limit(ang_velocity - baseang_velocity, angular_friction)
                            # print  np.dot(spd,perpendicular(direction))
